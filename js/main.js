$(function() {
  var updateDuration = function(element) {
    var interval;
    var startDuration = $(element).text().split(':');
    var startSeconds = parseInt(startDuration[0] * 60) + parseInt(startDuration[1]);
    var startTime = Date.now() / 1000;
    interval = setInterval(function() {
      var seconds = startSeconds - ((Date.now() / 1000) - startTime);
      if (seconds < 0) {
        clearInterval(interval);
        location = location
        return;
      }

      var m = parseInt(seconds / 60);
      var s = parseInt(seconds % 60);

      var padding = '';
      if (s < 10) {
        padding = 0;
      }

      $(element).text(m + ':' + padding + s);
    }, 1000);
  }

  $('span.duration').each(function(index, element) {updateDuration(element);});
  var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
  var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl, {trigger: 'hover focus click'});
  })
});
