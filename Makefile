build:
	go generate && go build -o codeGym main.go

install: build
	sudo cp ./codeGym /usr/bin/codeGym

run:
	go generate && go run main.go
