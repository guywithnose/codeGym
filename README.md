# Code Gym
A platform for programming contests, inspired by Project Eu;er.

### Running
To start the server run
```bash
$ go run main.go
```

While developing, it is recommended that you use [fresh](https://github.com/pilu/fresh).  Install with:
```bash
$ go get github.com/nsf/termbox-go
```

Then you can just run the server with
```bash
$ fresh
```

By default this will create a sqlite database in the current directory called `codeGym.sqlite` and start the server on port 4000.  The default admin password is `admin`.  All of these defaults can be overwritten with environment variables:

If you change any static files (including css, javascript, and templates), you will need to run [esc](https://github.com/mjibson/esc).  Once esc is installed you can just run:
```bash
$ go get github.com/mjibson/esc
$ go generate
```

### Environment variables
| Name                 | Description                                                                                          | Default               |
| -------------------- | ---------------------------------------------------------------------------------------------------- | --------------------- |
| BIND_ADDRESS         | The address and port of the server                                                                   | :4000                 |
| ADMIN_PASSWORD       | The password for the admin user                                                                      | admin                 |
| DATABASE_FILE        | The path to the sqlite database.  If it does not exist it will be created                            | ./tiEuler.sqlite      |
| APPLICATION_ENV      | The environment to use. Can be `production` or `development`                                         | development           |
| APP_NAME             | The name of the app                                                                                  | Code Gym              |
| TIME_ZONE            | The time zone to use for dates                                                                       | Local time zone       |
| SECURE_COOKIES       | If set to true, cookies will be set with secureOnly.  This will break the site if hosted without ssl | false                 |
| TELEGRAM_BOT_TOKEN   | The Telegram bot token to use for solve notifications.                                               |                       |
| TELEGRAM_CHAT_ID     | The Telegram chat to send notifications.                                                             |                       |
| **Mailgun settings** | Only used for password reset                                                                         |                       |
| MAILGUN_DOMAIN       | The mailgun domain to send from                                                                      |                       |
| MAILGUN_API_KEY      | The Mailgun API key                                                                                  |                       |
| BASE_URL             | The base URL.  Will be prepended to links sent in emails.                                            | http://localhost:4000 |
