//go:generate esc -pkg static -o static/static.go css fonts js templates
package main

import (
	"log"
	"os"
	"strings"
	"time"

	"gitlab.com/guywithnose/codeGym/app"

	"github.com/go-redis/redis"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func main() {
	address, addressSet := os.LookupEnv("BIND_ADDRESS")
	if !addressSet {
		address = ":4000"
	}

	databaseFile, databaseFileSet := os.LookupEnv("DATABASE_FILE")
	if !databaseFileSet {
		databaseFile = "./codeGym.sqlite"
	}

	baseURL, baseURLSet := os.LookupEnv("BASE_URL")
	if !baseURLSet {
		baseURL = "http://localhost:4000"
	}

	appName, appNameSet := os.LookupEnv("APP_NAME")
	if !appNameSet {
		appName = "Code Gym"
	}

	adminPassword := os.Getenv("ADMIN_PASSWORD")

	cache := redis.NewClient(&redis.Options{Addr: "localhost:6379"})

	locationName, locationNameSet := os.LookupEnv("TIME_ZONE")
	location := time.Local
	if locationNameSet {
		var err error
		location, err = time.LoadLocation(locationName)
		if err != nil {
			location = time.Local
		}
	}

	mailgunDomain, mailgunDomainSet := os.LookupEnv("MAILGUN_DOMAIN")
	mailgunAPIKey, mailgunAPIKeySet := os.LookupEnv("MAILGUN_APIKEY")
	var mgm app.MailGunMailerInterface
	if mailgunDomainSet && mailgunAPIKeySet {
		mgm = app.NewMailGunMailer(mailgunDomain, mailgunAPIKey)
	}

	telegramBotToken, telegramBotTokenSet := os.LookupEnv("TELEGRAM_BOT_TOKEN")
	telegramChatId, telegramChatIdSet := os.LookupEnv("TELEGRAM_CHAT_ID")
	var telegram app.TelegramNotifierInterface
	if telegramBotTokenSet && telegramChatIdSet {
		telegram = app.NewTelegramNotifier(telegramBotToken, telegramChatId)
	}

	hideScoreboard := os.Getenv("HIDE_SCOREBOARD")

	validUsersList := os.Getenv("VALID_NAMES")
	validUsers := strings.Split(validUsersList, ",")

	app := app.New(address, databaseFile, appName, os.Getenv("APPLICATION_ENV"), adminPassword, baseURL, cache, mgm, location, telegram, hideScoreboard == "true", validUsers)
	log.Fatal(app.Run())
}
