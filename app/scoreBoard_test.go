package app

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/stretchr/testify/assert"
)

func setUp(t *testing.T) (*App, string) {
	app, db, databaseFile := getApp(t)
	before, _ := time.ParseDuration("-1m")
	after, _ := time.ParseDuration("1m")

	u := &user{Username: "un"}
	db.Save(&u)
	u2 := &user{Username: "un2"}
	db.Save(&u2)
	u3 := &user{Username: "un3"}
	db.Save(&u3)
	p := &problem{Name: "Name", Description: "Description", Answer: 500, StartDate: time.Now().Add(before)}
	db.Save(&p)
	a := &attempt{UserID: u.ID, ProblemID: p.ID, Success: true}
	db.Save(&a)
	p2 := &problem{Name: "Name2", Description: "Description", Answer: 500, StartDate: time.Now().Add(after)}
	db.Save(&p2)
	a2 := &attempt{UserID: u2.ID, ProblemID: p2.ID, Success: true}
	db.Save(&a2)
	a3 := &attempt{UserID: u.ID, ProblemID: p2.ID, Success: true}
	db.Save(&a3)
	p3 := &problem{Name: "NoShow", Description: "Description", Answer: 500, StartDate: time.Now().Add(after)}
	db.Save(&p3)

	return app, databaseFile
}

func TestScoreBoard(t *testing.T) {
	app, databaseFile := setUp(t)
	defer os.Remove(databaseFile)
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, routeScoreBoard, nil)
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("un")))
	app.scoreBoard(w, r)

	assert.Contains(t, stripWhitespace(w.Body.String()), fmt.Sprintf("<tr><td><ahref=\"%sun\">un</a></td><td>2</td></tr><tr><td><ahref=\"%sun2\">un2</a></td><td>1</td></tr>", routeScoreBoard, routeScoreBoard))
}

func TestScoreBoardUser(t *testing.T) {
	app, databaseFile := setUp(t)
	defer os.Remove(databaseFile)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("%sun", routeScoreBoard), nil)
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("un")))
	app.scoreBoard(w, r)

	assert.Contains(t, stripWhitespace(w.Body.String()), "<tr><td>Name2</td><td>")
	assert.Contains(t, stripWhitespace(w.Body.String()), "</tr><tr><td>Name</td><td>")
	assert.NotContains(t, stripWhitespace(w.Body.String()), "<tr><td>NoShow</td><td>")

	w = httptest.NewRecorder()
	r = httptest.NewRequest(http.MethodGet, fmt.Sprintf("%sun2", routeScoreBoard), nil)
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("un")))
	app.scoreBoard(w, r)

	assert.Contains(t, stripWhitespace(w.Body.String()), "<tr><td>Name2</td><td>")
	assert.NotContains(t, stripWhitespace(w.Body.String()), "<tr><td>NoShow</td><td>")
}

func TestScoreBoardInvalidUser(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("%snotAUser", routeScoreBoard), nil)
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("un")))
	app.scoreBoard(w, r)

	assertGetRedirect(t, w, routeScoreBoard)
}
