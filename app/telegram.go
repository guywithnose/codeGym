package app

import (
	"fmt"
	"net/http"
	"net/url"
)

type TelegramNotifierInterface interface {
	Send(text string)
}

type telegramNotifier struct {
	apiToken string
	chatID   string
}

func NewTelegramNotifier(apiToken, chatID string) *telegramNotifier {
	return &telegramNotifier{apiToken: apiToken, chatID: chatID}
}

func (telegram telegramNotifier) Send(text string) {
	queryParams := url.Values{
		"chat_id": []string{telegram.chatID},
		"text":    []string{text},
	}

	url := fmt.Sprintf("https://api.telegram.org/bot%s/sendMessage?%s", telegram.apiToken, queryParams.Encode())
	_, err := http.Get(url)
	if err != nil {
		fmt.Printf("Unable to notify about solve: %v\n", err)
	}
}
