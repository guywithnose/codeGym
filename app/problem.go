package app

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func (a *App) problem(w http.ResponseWriter, r *http.Request) {
	viewData := a.getTemplateData("problem", r)

	if viewData.User == nil {
		http.Redirect(w, r, routeLogin, http.StatusFound)
		return
	}

	lastRouteSegment := strings.Replace(r.URL.String(), routeProblem, "", -1)
	if lastRouteSegment == "" {
		http.Redirect(w, r, routeIndex, http.StatusFound)
		return
	}

	problemID, err := strconv.Atoi(lastRouteSegment)
	if err != nil {
		http.Redirect(w, r, routeIndex, http.StatusFound)
		return
	}

	for _, p := range viewData.AllProblems {
		if p.ID == uint(problemID) && (p.isActive || p.isArchived) {
			a.handleProblem(viewData, r, p)
			panicOnError(a.templates.ExecuteTemplate(w, "problemDetail", viewData))
			return
		}
	}

	http.Redirect(w, r, routeIndex, http.StatusFound)
}

func (a *App) handleProblem(viewData *templateData, r *http.Request, p *problem) {
	if p.isArchived {
		viewData.IsArchivedProblem = true
	}

	viewData.CurrentProblem = &problem{}
	*viewData.CurrentProblem = *p
	viewData.LastAttempt = &attempt{}
	err := a.db.First(viewData.LastAttempt, "user_id = ? AND problem_id = ? AND success = 1", viewData.User.ID, p.ID)
	if err != nil {
		a.db.Order("created_at desc").First(viewData.LastAttempt, "user_id = ? AND problem_id = ?", viewData.User.ID, p.ID)
	}

	nextSubmissionDuration := time.Until(viewData.LastAttempt.CreatedAt.Add(submitFrequency))
	viewData.CanSubmitProblem = !viewData.LastAttempt.Success && nextSubmissionDuration < 0 && p.isActive

	mappedUsers := getMappedUsers(a.db)
	if r.Method == http.MethodPost && viewData.CanSubmitProblem {
		a.handleProblemPost(viewData, r, p, mappedUsers)
	}

	if viewData.LastAttempt.Success || viewData.IsAdmin || p.isArchived {
		a.getSharedCode(viewData, p.ID, mappedUsers)
	} else if nextSubmissionDuration > 0 {
		viewData.NextSubmissionDuration = fmt.Sprintf("%d:%02d", int(nextSubmissionDuration.Minutes()), int(nextSubmissionDuration.Seconds())%60)
	}

	var solves []*attempt
	a.db.Order("created_at asc").Find(&solves, "problem_id = ? AND success = 1", p.ID)
	viewData.ProblemSolves = []string{}
	for _, solve := range solves {
		username := mappedUsers[solve.UserID]
		if username != adminUser {
			viewData.ProblemSolves = append(viewData.ProblemSolves, username)
		}
	}
}

func (a *App) getSharedCode(viewData *templateData, problemID uint, mappedUsers map[uint]string) {
	var shares []sharedCode
	a.db.Order("created_at asc").Find(&shares, "problem_id = ?", problemID)

	viewData.Shares = []share{}
	for _, s := range shares {
		if mappedUsers[s.UserID] != adminUser {
			viewData.Shares = append(viewData.Shares, share{Username: mappedUsers[s.UserID], Link: s.Link})
		}
	}
}

func (a *App) handleProblemPost(viewData *templateData, r *http.Request, p *problem, mappedUsers map[uint]string) {
	answer, err := strconv.Atoi(r.FormValue("answer"))
	if err != nil {
		viewData.Error = invalidAnswer
	} else {
		success := p.Answer == answer
		viewData.LastAttempt = &attempt{
			UserID:    viewData.User.ID,
			ProblemID: p.ID,
			Value:     answer,
			Success:   success,
		}

		a.db.Create(&viewData.LastAttempt)

		viewData.CanSubmitProblem = false
		if !success {
			viewData.Error = invalidAnswer
			viewData.NextSubmissionDuration = "5:00"
		} else if a.telegram != nil {
			mappedProblems := getMappedProblems(viewData.AllProblems)
			text := fmt.Sprintf("%s solved %s", mappedUsers[viewData.User.ID], mappedProblems[p.ID])
			a.telegram.Send(text)
		}
	}
}
