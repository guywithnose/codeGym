package app

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/stretchr/testify/assert"
)

func TestChangePasswordNoLogin(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	w := httptest.NewRecorder()
	params := url.Values{"oldPassword": {"oldPassword"}, "newPassword": {"newPass"}, "confirmPassword": {"newPass"}}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, routeChangePassword, body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	app.changePassword(w, r)
	assert.Equal(t, "", w.Body.String())
	assert.Equal(t, []string{routeIndex}, w.HeaderMap["Location"])
}

func TestChangePassword(t *testing.T) {
	w, db, databaseFile := getChangePasswordResponse(t, url.Values{"oldPassword": {"oldPassword"}, "newPassword": {"newPass"}, "confirmPassword": {"newPass"}})
	defer os.Remove(databaseFile)
	assert.Equal(t, "", w.Body.String())
	assert.Equal(t, []string{routeIndex}, w.HeaderMap["Location"])

	u2 := &user{}
	db.First(u2, "username = ?", "user")

	assert.True(t, verifyLogin(u2, "newPass"))
}

func TestChangePasswordWrongPassword(t *testing.T) {
	w, _, databaseFile := getChangePasswordResponse(t, url.Values{"oldPassword": {"oldPas"}, "newPassword": {"newPass"}, "confirmPassword": {"newPass"}})
	defer os.Remove(databaseFile)
	assert.Contains(t, w.Body.String(), wrongPassword)
}

func TestChangePasswordNoMatch(t *testing.T) {
	w, _, databaseFile := getChangePasswordResponse(t, url.Values{"oldPassword": {"oldPassword"}, "newPassword": {"newPass"}, "confirmPassword": {"newPassword"}})
	defer os.Remove(databaseFile)
	assert.Contains(t, w.Body.String(), passwordsDoNotMatch)
}

func TestChangeFormNoLogin(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, routeChangePassword, nil)
	app.changePassword(w, r)
	assertGetRedirect(t, w, routeIndex)
}

func TestChangeForm(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	salt, hash := generateHash("oldPassword")
	u := &user{Username: "user", Email: "email", Salt: salt, Hash: hash}
	db.Save(&u)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, routeChangePassword, nil)
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("user")))
	app.changePassword(w, r)
	assert.Contains(t, w.Body.String(), "Old Password")
	assert.Contains(t, w.Body.String(), "New Password")
}

func getChangePasswordResponse(t *testing.T, params url.Values) (*httptest.ResponseRecorder, *gorm.DB, string) {
	app, db, databaseFile := getApp(t)

	salt, hash := generateHash("oldPassword")
	u := &user{Username: "user", Email: "email", Salt: salt, Hash: hash}
	db.Save(&u)

	w := httptest.NewRecorder()
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, routeChangePassword, body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("user")))
	app.changePassword(w, r)

	return w, db, databaseFile
}
