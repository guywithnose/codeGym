package app

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strconv"
	"strings"
	"testing"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/stretchr/testify/assert"
)

func TestShareCodeNoLogin(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	p := &problem{Name: "problem name", Description: "problem description", Answer: 30}
	db.Save(&p)

	w := httptest.NewRecorder()
	params := url.Values{"problemID": {strconv.Itoa(int(p.ID))}, "link": {"https://www.google.com/search?q=how+to+solve+a+problem"}}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, routeShareCode, body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	app.shareCode(w, r)
	assert.Equal(t, "", w.Body.String())
	assert.Equal(t, []string{routeLogin}, w.HeaderMap["Location"])
}

func TestShareCodeGet(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	p := &problem{Name: "problem name", Description: "problem description", Answer: 30}
	db.Save(&p)

	assertRouteReturnsRedirectIndex(t, app, routeShareCode, "un", app.shareCode)
}

func TestShareCode(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	u := &user{Username: "un"}
	db.Save(&u)
	p := &problem{Name: "Name", Description: "Description", Answer: 500}
	db.Save(&p)

	w := httptest.NewRecorder()
	params := url.Values{"problemID": {strconv.Itoa(int(p.ID))}, "link": {"https://www.google.com/search?q=how+to+solve+a+problem"}}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, routeShareCode, body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("un")))
	app.shareCode(w, r)
}

func TestShareCodeInvalidProblem(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	u := &user{Username: "un"}
	db.Save(&u)

	w := httptest.NewRecorder()
	params := url.Values{"problemID": {"foo"}, "link": {"https://www.google.com/search?q=how+to+solve+a+problem"}}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, routeShareCode, body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("un")))
	app.shareCode(w, r)
	assert.Equal(t, "", w.Body.String())
	assert.Equal(t, []string{routeIndex}, w.HeaderMap["Location"])
}
