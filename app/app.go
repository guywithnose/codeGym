package app

import (
	"html/template"
	"net/http"
	"time"

	"github.com/go-redis/redis"
	"github.com/jinzhu/gorm"
	"gitlab.com/guywithnose/codeGym/static"
)

const (
	saltLength      = 64
	hashLength      = 128
	tokenLength     = 128
	tokenLifetime   = time.Hour * 24
	submitFrequency = time.Minute * 5
)

// App is the application
type App struct {
	db             *gorm.DB
	server         *http.Server
	cache          *redis.Client
	templates      *template.Template
	mailer         mailer
	baseURL        string
	appName        string
	location       *time.Location
	telegram       TelegramNotifierInterface
	hideScoreboard bool
	validUsers     []string
}

type mailer interface {
	Send(subject, body, to string)
}

// New returns a new server
func New(
	addr,
	databaseFile,
	appName,
	environment,
	adminPassword,
	url string,
	cache *redis.Client,
	m mailer,
	location *time.Location,
	telegram TelegramNotifierInterface,
	hideScoreboard bool,
	validUsers []string,
) *App {
	fileServer := http.FileServer(static.FS(environment != "production"))
	templates, err := getTemplates(environment)
	panicOnError(err)

	handler := &http.ServeMux{}
	db, err := gorm.Open("sqlite3", databaseFile)
	panicOnError(err)

	db.AutoMigrate(&user{}, &problem{}, &attempt{}, &sharedCode{})

	app := &App{
		db:             db,
		server:         &http.Server{Addr: addr, Handler: handler},
		cache:          cache,
		templates:      templates,
		mailer:         m,
		baseURL:        url,
		appName:        appName,
		location:       location,
		telegram:       telegram,
		hideScoreboard: hideScoreboard,
		validUsers:     validUsers,
	}

	handler.Handle("/css/", fileServer)
	handler.Handle("/fonts/", fileServer)
	handler.Handle("/js/", fileServer)
	handler.HandleFunc(routeIndex, app.index)
	handler.HandleFunc(routeLogin, app.login)
	handler.HandleFunc(routeLogout, app.logout)
	handler.HandleFunc(routeProblem, app.problem)
	handler.HandleFunc(routeEditProblem, app.editProblem)
	handler.HandleFunc(routeNewProblem, app.newProblem)
	handler.HandleFunc(routeNewUser, app.newUser)
	handler.HandleFunc(routeShareCode, app.shareCode)
	handler.HandleFunc(routeResetPasword, app.resetPassword)
	handler.HandleFunc(routeChangePassword, app.changePassword)
	handler.HandleFunc(routeScoreBoard, app.scoreBoard)
	handler.HandleFunc(routeEditUser, app.editUser)

	updateAdminPassword(db, adminPassword)

	return app
}

// Run runs the app
func (a *App) Run() error {
	defer a.db.Close()
	return a.server.ListenAndServe()
}

func panicOnError(err error) {
	if err != nil {
		panic(err)
	}
}
