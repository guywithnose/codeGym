package app

import (
	"net/http"
	"time"
)

func (a *App) logout(w http.ResponseWriter, r *http.Request) {
	authCookie := &http.Cookie{Name: authCookieName, Expires: time.Now().Add(-time.Hour * 24), HttpOnly: true, Path: routeIndex}
	http.SetCookie(w, authCookie)
	http.Redirect(w, r, routeIndex, http.StatusFound)
}
