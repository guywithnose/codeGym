package app

import (
	"net/http"
)

func (a *App) index(w http.ResponseWriter, r *http.Request) {
	if r.URL.RequestURI() != routeIndex {
		http.Redirect(w, r, routeIndex, http.StatusFound)
		return
	}

	viewData := a.getTemplateData("index", r)
	panicOnError(a.templates.ExecuteTemplate(w, "index", viewData))
}
