package app

import (
	"net/http"
)

func (a *App) login(w http.ResponseWriter, r *http.Request) {
	viewData := a.getTemplateData("login", r)
	if viewData.User != nil {
		http.Redirect(w, r, routeIndex, http.StatusFound)
		return
	}

	if r.Method == http.MethodPost {
		username := r.FormValue("username")
		password := r.FormValue("password")
		u := &user{}
		a.db.First(u, "username = ? OR email = ?", username, username)

		if verifyLogin(u, password) {
			http.SetCookie(w, a.generateAuthCookie(u.Username))
			http.Redirect(w, r, routeIndex, http.StatusFound)
			return
		}

		viewData.Error = "Invalid username or password"
	}

	panicOnError(a.templates.ExecuteTemplate(w, "login", viewData))
}
