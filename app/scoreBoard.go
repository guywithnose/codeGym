package app

import (
	"net/http"
	"sort"
	"strings"
	"time"
)

func (a *App) scoreBoard(w http.ResponseWriter, r *http.Request) {
	viewData := a.getTemplateData("scoreBoard", r)

	var solves []attempt

	userName := strings.Replace(r.URL.String(), routeScoreBoard, "", -1)
	if userName == "" {
		a.handleMainScoreBoard(viewData, solves)
		panicOnError(a.templates.ExecuteTemplate(w, "scoreBoard", viewData))
		return
	}

	var u user
	err := a.db.Find(&u, "username = ?", userName).Error
	if err != nil {
		http.Redirect(w, r, routeScoreBoard, http.StatusFound)
		return
	}

	problemSolves := map[string]*time.Time{}
	viewData.Solves = []problemSolve{}
	a.db.Find(&solves, "success = 1 and user_id = ?", u.ID)
	now := time.Now().In(a.location)
	for _, p := range viewData.AllProblems {
		if now.After(p.StartDate) {
			problemSolves[p.Name] = nil
		}
	}

	mappedProblems := getMappedProblems(viewData.AllProblems)
	for _, solve := range solves {
		date := solve.CreatedAt
		problemSolves[mappedProblems[solve.ProblemID]] = &date
	}

	viewData.Solves = []problemSolve{}
	for problemName, solvedDate := range problemSolves {
		viewData.Solves = append(viewData.Solves, problemSolve{ProblemName: problemName, Date: solvedDate})
	}

	sortFunc := func(i, j int) bool {
		if viewData.Solves[i].Date == nil {
			return false
		}

		if viewData.Solves[j].Date == nil {
			return true
		}

		return viewData.Solves[i].Date.After(*viewData.Solves[j].Date)
	}

	sort.Slice(viewData.Solves, sortFunc)

	panicOnError(a.templates.ExecuteTemplate(w, "scoreBoardUser", viewData))
}

func (a *App) handleMainScoreBoard(viewData *templateData, solves []attempt) {
	a.db.Find(&solves, "success = 1")

	scores := map[string]map[string]bool{}
	mappedUsers := getMappedUsers(a.db)
	mappedProblems := getMappedProblems(viewData.AllProblems)
	for _, solve := range solves {
		if solve.UserID != 0 && mappedUsers[solve.UserID] != adminUser {
			if _, ok := scores[mappedUsers[solve.UserID]]; !ok {
				scores[mappedUsers[solve.UserID]] = map[string]bool{}
			}

			scores[mappedUsers[solve.UserID]][mappedProblems[solve.ProblemID]] = true
		}
	}

	viewData.Scores = []score{}
	for username, solves := range scores {
		viewData.Scores = append(viewData.Scores, score{Username: username, Solves: len(solves)})
	}

	sortFunc := func(i, j int) bool {
		if viewData.Scores[i].Solves == viewData.Scores[j].Solves {
			return viewData.Scores[i].Username < viewData.Scores[j].Username
		}

		return viewData.Scores[i].Solves > viewData.Scores[j].Solves
	}

	sort.Slice(viewData.Scores, sortFunc)
}
