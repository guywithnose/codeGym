package app

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestResetPasswordAlreadyAuthenticated(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, routeResetPasword, nil)
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken(adminUser)))
	app.resetPassword(w, r)
	assertGetRedirect(t, w, routeIndex)
}

func TestResetPassword(t *testing.T) {
	app, db, databaseFile := getApp(t)
	app.mailer = &testMailer{}
	defer os.Remove(databaseFile)

	salt, hash := generateHash("oldPassword")
	u := &user{Username: "user", Email: "email", Salt: salt, Hash: hash}
	db.Save(&u)

	w := httptest.NewRecorder()
	params := url.Values{"username": {"user"}}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, routeResetPasword, body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	app.resetPassword(w, r)
	assert.Contains(t, w.Body.String(), "Check your email for a reset password link.")

	require.Equal(t, 1, len(app.mailer.(*testMailer).messages))
	assert.Equal(t, "email", fmt.Sprintf("%v", app.mailer.(*testMailer).messages[0].to))
	assert.Equal(t, "Reset Password", fmt.Sprintf("%v", app.mailer.(*testMailer).messages[0].subject))
	require.Contains(t, fmt.Sprintf("%v", app.mailer.(*testMailer).messages[0].body), "token=")

	token, err := url.QueryUnescape(strings.Split(strings.Split(app.mailer.(*testMailer).messages[0].body, "=")[1], " ")[0])
	require.Nil(t, err)
	w = httptest.NewRecorder()
	postParams := url.Values{"newPassword": {"newPass"}, "confirmPassword": {"newPass"}}
	getParams := url.Values{"token": {token}}
	body = strings.NewReader(postParams.Encode())
	r = httptest.NewRequest(http.MethodPost, fmt.Sprintf("%s?%s", routeResetPasword, getParams.Encode()), body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	app.resetPassword(w, r)
	require.Equal(t, 1, len(w.HeaderMap["Set-Cookie"]))
	assert.Contains(t, w.HeaderMap["Set-Cookie"][0], fmt.Sprintf("%s=", authCookieName))
	assert.Equal(t, []string{routeIndex}, w.HeaderMap["Location"])
}

func TestResetPasswordNoMatch(t *testing.T) {
	app, db, databaseFile := getApp(t)
	app.mailer = &testMailer{}
	defer os.Remove(databaseFile)

	salt, hash := generateHash("oldPassword")
	u := &user{Username: "user", Email: "email", Salt: salt, Hash: hash}
	db.Save(&u)

	w := httptest.NewRecorder()
	params := url.Values{"username": {"user"}}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, routeResetPasword, body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	app.resetPassword(w, r)
	assert.Contains(t, w.Body.String(), "Check your email for a reset password link.")

	require.Equal(t, 1, len(app.mailer.(*testMailer).messages))
	assert.Equal(t, "email", fmt.Sprintf("%v", app.mailer.(*testMailer).messages[0].to))
	assert.Equal(t, "Reset Password", fmt.Sprintf("%v", app.mailer.(*testMailer).messages[0].subject))
	require.Contains(t, fmt.Sprintf("%v", app.mailer.(*testMailer).messages[0].body), "token=")

	token, err := url.QueryUnescape(strings.Split(strings.Split(app.mailer.(*testMailer).messages[0].body, "=")[1], " ")[0])
	require.Nil(t, err)
	w = httptest.NewRecorder()
	postParams := url.Values{"newPassword": {"newPass"}, "confirmPassword": {"newPass2"}}
	getParams := url.Values{"token": {token}}
	body = strings.NewReader(postParams.Encode())
	r = httptest.NewRequest(http.MethodPost, fmt.Sprintf("%s?%s", routeResetPasword, getParams.Encode()), body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	app.resetPassword(w, r)
	assert.Contains(t, w.Body.String(), passwordsDoNotMatch)
}

func TestResetPasswordBadToken(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	w := httptest.NewRecorder()
	postParams := url.Values{"newPassword": {"newPass"}, "confirmPassword": {"newPass2"}}
	getParams := url.Values{"token": {"badToken"}}
	body := strings.NewReader(postParams.Encode())
	r := httptest.NewRequest(http.MethodPost, fmt.Sprintf("%s?%s", routeResetPasword, getParams.Encode()), body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	app.resetPassword(w, r)
	assert.Equal(t, "", w.Body.String())
	assert.Equal(t, []string{routeIndex}, w.HeaderMap["Location"])
}

func TestResetPasswordNoUser(t *testing.T) {
	app, db, databaseFile := getApp(t)
	app.mailer = &testMailer{}
	defer os.Remove(databaseFile)

	salt, hash := generateHash("oldPassword")
	u := &user{Username: "user", Email: "email", Salt: salt, Hash: hash}
	db.Save(&u)

	w := httptest.NewRecorder()
	params := url.Values{"username": {"user"}}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, routeResetPasword, body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	app.resetPassword(w, r)
	assert.Contains(t, w.Body.String(), "Check your email for a reset password link.")

	require.Equal(t, 1, len(app.mailer.(*testMailer).messages))
	assert.Equal(t, "email", fmt.Sprintf("%v", app.mailer.(*testMailer).messages[0].to))
	assert.Equal(t, "Reset Password", fmt.Sprintf("%v", app.mailer.(*testMailer).messages[0].subject))
	require.Contains(t, fmt.Sprintf("%v", app.mailer.(*testMailer).messages[0].body), "token=")

	db.Delete(u)

	token, err := url.QueryUnescape(strings.Split(strings.Split(app.mailer.(*testMailer).messages[0].body, "=")[1], " ")[0])
	require.Nil(t, err)
	w = httptest.NewRecorder()
	postParams := url.Values{"newPassword": {"newPass"}, "confirmPassword": {"newPass2"}}
	getParams := url.Values{"token": {token}}
	body = strings.NewReader(postParams.Encode())
	r = httptest.NewRequest(http.MethodPost, fmt.Sprintf("%s?%s", routeResetPasword, getParams.Encode()), body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	app.resetPassword(w, r)
	assert.Equal(t, "", w.Body.String())
	assert.Equal(t, []string{routeIndex}, w.HeaderMap["Location"])
}

func TestResetForm(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, routeResetPasword, nil)
	app.resetPassword(w, r)
	assert.Contains(t, w.Body.String(), "Username")
}
