package app

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/stretchr/testify/assert"
)

func TestLogout(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, routeLogout, nil)
	app.logout(w, r)
	assertGetRedirect(t, w, routeIndex)
	assert.Equal(t, 1, len(w.HeaderMap["Set-Cookie"]))
	assert.Contains(t, w.HeaderMap["Set-Cookie"][0], fmt.Sprintf("%s=", authCookieName))
}
