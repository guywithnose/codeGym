package app

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/stretchr/testify/assert"
)

func TestNewUserNoLogin(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	w := httptest.NewRecorder()
	params := url.Values{"username": {"un"}, "email": {"foo@bar.com"}, "password": {"pass"}, "confirmPassword": {"pass"}}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, routeNewUser, body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	app.newUser(w, r)
	assert.Equal(t, "", w.Body.String())
	assert.Equal(t, []string{routeIndex}, w.HeaderMap["Location"])
}

func TestNewUser(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	w := httptest.NewRecorder()
	params := url.Values{"username": {"un"}, "email": {"foo@bar.com"}, "password": {"pass"}, "confirmPassword": {"pass"}}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, routeNewUser, body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken(adminUser)))
	app.newUser(w, r)
	assert.Equal(t, "", w.Body.String())
	assert.Equal(t, 1, len(w.HeaderMap["Location"]))
	assert.Contains(t, w.HeaderMap["Location"][0], routeEditUser)

	u := &user{}
	db.First(u, "id = ?", 2)

	assert.Equal(t, "un", u.Username)
	assert.Equal(t, "foo@bar.com", u.Email)
	assert.False(t, u.IsAdmin)

	assert.True(t, verifyLogin(u, "pass"))
}

func TestNewUserMisMatchedPasswords(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	w := httptest.NewRecorder()
	params := url.Values{"username": {"un"}, "email": {"foo@bar.com"}, "password": {"pass"}, "confirmPassword": {"PASS"}}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, routeNewUser, body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken(adminUser)))
	app.newUser(w, r)
	assert.Contains(t, w.Body.String(), passwordsDoNotMatch)
}

func TestNewUserNonAdmin(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	w := httptest.NewRecorder()
	params := url.Values{"username": {"un"}, "email": {"foo@bar.com"}, "password": {"pass"}, "confirmPassword": {"pass"}}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, routeNewUser, body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("notAdmin")))
	app.newUser(w, r)
	assert.Equal(t, "", w.Body.String())
	assert.Equal(t, []string{routeIndex}, w.HeaderMap["Location"])
}

func TestNewUserForm(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, routeNewUser, nil)
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken(adminUser)))
	app.newUser(w, r)
	assert.Contains(t, w.Body.String(), "Username")
	assert.Contains(t, w.Body.String(), "Email")
	assert.Contains(t, w.Body.String(), "Password")
}
