package app

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

func (a *App) editUser(w http.ResponseWriter, r *http.Request) {
	viewData := a.getTemplateData("editUser", r)
	if !viewData.IsAdmin {
		http.Redirect(w, r, routeIndex, http.StatusFound)
		return
	}

	a.db.Find(&viewData.AllUsers)
	lastRouteSegment := strings.Replace(r.URL.String(), routeEditUser, "", -1)
	if lastRouteSegment == "" {
		panicOnError(a.templates.ExecuteTemplate(w, "userList", viewData))
		return
	}

	userID, err := strconv.Atoi(lastRouteSegment)
	if err != nil {
		http.Redirect(w, r, routeEditUser, http.StatusFound)
		return
	}

	for _, u := range viewData.AllUsers {
		if u.ID == uint(userID) {
			viewData.CurrentUser = &user{}
			*viewData.CurrentUser = *u
			if r.Method == http.MethodPost {
				handleEditUserPost(viewData, r)

				if viewData.Error == "" {
					a.db.Save(viewData.CurrentUser)
					http.Redirect(w, r, fmt.Sprintf("%s%d", routeEditUser, u.ID), http.StatusFound)
					return
				}
			}

			panicOnError(a.templates.ExecuteTemplate(w, "editUser", viewData))
			return
		}
	}

	http.Redirect(w, r, routeEditUser, http.StatusFound)
}

func handleEditUserPost(viewData *templateData, r *http.Request) {
	username := r.FormValue("username")
	email := r.FormValue("email")
	newPassword := r.FormValue("newPassword")
	confirmPassword := r.FormValue("confirmPassword")

	viewData.CurrentUser.Username = username
	viewData.CurrentUser.Email = email

	if newPassword == "" && confirmPassword == "" {
		return
	}

	if newPassword != confirmPassword {
		viewData.Error = passwordsDoNotMatch
		return
	}

	salt, hash := generateHash(newPassword)
	viewData.CurrentUser.Hash = hash
	viewData.CurrentUser.Salt = salt
}
