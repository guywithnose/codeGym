package app

import (
	"fmt"
	"html/template"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/stretchr/testify/assert"
)

func TestEditProblemNoLogin(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	p := &problem{Name: "Old Name", Description: "Old Description", Answer: 3}
	db.Save(&p)

	w := httptest.NewRecorder()
	params := url.Values{
		"name":        {"new name"},
		"description": {"new description"},
		"answer":      {"70"},
		"startDate":   {"4/7/18"},
		"endDate":     {"4/21/18"},
	}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, fmt.Sprintf("%s%d", routeEditProblem, p.ID), body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	app.editProblem(w, r)
	assert.Equal(t, "", w.Body.String())
	assert.Equal(t, []string{routeIndex}, w.HeaderMap["Location"])
}

func TestEditProblem(t *testing.T) {
	w, db := getEditProblemPostResponse(
		t,
		url.Values{
			"name":        {"new name"},
			"description": {"new description"},
			"answer":      {"70"},
			"startDate":   {"4/7/18"},
			"endDate":     {"4/21/18"},
		},
	)
	assert.Equal(t, "", w.Body.String())
	assert.Equal(t, []string{fmt.Sprintf("%s1", routeProblem)}, w.HeaderMap["Location"])

	updatedProblem := &problem{}
	db.First(updatedProblem, "id = ?", 1)

	assert.Equal(t, "new name", updatedProblem.Name)
	assert.Equal(t, template.HTML("new description"), updatedProblem.Description)
	assert.Equal(t, 70, updatedProblem.Answer)
}

func TestEditProblemBadAnswer(t *testing.T) {
	w, _ := getEditProblemPostResponse(
		t,
		url.Values{
			"name":        {"new name"},
			"description": {"new description"},
			"answer":      {"notAnInt"},
			"startDate":   {"4/7/18"},
			"endDate":     {"4/21/18"},
		},
	)
	assert.Contains(t, w.Body.String(), invalidAnswer)
}

func TestEditProblemBadStartDate(t *testing.T) {
	w, _ := getEditProblemPostResponse(
		t,
		url.Values{
			"name":        {"new name"},
			"description": {"new description"},
			"answer":      {"70"},
			"startDate":   {"notaDate"},
			"endDate":     {"4/21/18"},
		},
	)
	assert.Contains(t, w.Body.String(), badStartDate)
}

func TestEditProblemBadEndDate(t *testing.T) {
	w, _ := getEditProblemPostResponse(
		t,
		url.Values{
			"name":        {"new name"},
			"description": {"new description"},
			"answer":      {"70"},
			"startDate":   {"4/7/18"},
			"endDate":     {"notADate"},
		},
	)
	assert.Contains(t, w.Body.String(), badEndDate)
}

func TestEditProblemNonAdmin(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	p := &problem{Name: "Old Name", Description: "Old Description", Answer: 3}
	db.Save(&p)

	w := httptest.NewRecorder()
	params := url.Values{
		"name":        {"new name"},
		"description": {"new description"},
		"answer":      {"70"},
		"startDate":   {"4/7/18"},
		"endDate":     {"4/21/18"},
	}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, fmt.Sprintf("%s%d", routeEditProblem, p.ID), body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("notAdmin")))
	app.editProblem(w, r)
	assert.Equal(t, "", w.Body.String())
	assert.Equal(t, []string{routeIndex}, w.HeaderMap["Location"])
}

func TestEditProblemForm(t *testing.T) {
	w, databaseFile := getEditProblemGetResponse(t, fmt.Sprintf("%s1", routeEditProblem))
	defer os.Remove(databaseFile)

	assert.Contains(t, w.Body.String(), "Name")
	assert.Contains(t, w.Body.String(), "Description")
}

func TestEditProblemFormInvalidProblem(t *testing.T) {
	w, databaseFile := getEditProblemGetResponse(t, fmt.Sprintf("%sfff", routeEditProblem))
	defer os.Remove(databaseFile)

	assertGetRedirect(t, w, routeEditProblem)
}

func TestEditProblemFormNonExistentProblem(t *testing.T) {
	w, databaseFile := getEditProblemGetResponse(t, fmt.Sprintf("%s999", routeEditProblem))
	defer os.Remove(databaseFile)

	assertGetRedirect(t, w, routeEditProblem)
}

func TestEditProblemList(t *testing.T) {
	w, databaseFile := getEditProblemGetResponse(t, routeEditProblem)
	defer os.Remove(databaseFile)

	assert.Contains(t, w.Body.String(), "Old Name")
	assert.Contains(t, w.Body.String(), "New Problem")
}

func getEditProblemGetResponse(t *testing.T, url string) (*httptest.ResponseRecorder, string) {
	app, db, databaseFile := getApp(t)

	p := &problem{Name: "Old Name", Description: "Old Description", Answer: 3}
	db.Save(&p)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, url, nil)
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken(adminUser)))
	app.editProblem(w, r)

	return w, databaseFile
}

func getEditProblemPostResponse(t *testing.T, params url.Values) (*httptest.ResponseRecorder, *gorm.DB) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	p := &problem{Name: "Old Name", Description: "Old Description", Answer: 3}
	db.Save(&p)

	w := httptest.NewRecorder()
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, fmt.Sprintf("%s%d", routeEditProblem, p.ID), body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken(adminUser)))
	app.editProblem(w, r)

	return w, db
}
