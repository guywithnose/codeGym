package app

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/stretchr/testify/assert"
)

func TestEditUserNoLogin(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	salt, hash := generateHash("oldPassword")
	u := &user{Username: "Old Username", Email: "Old Email", Hash: hash, Salt: salt}
	db.Save(&u)

	w := httptest.NewRecorder()
	params := url.Values{
		"username":        {"new username"},
		"email":           {"new email"},
		"newPassword":     {"newPassword"},
		"confirmPassword": {"newPassword"},
	}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, fmt.Sprintf("%s%d", routeEditUser, u.ID), body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	app.editUser(w, r)
	assert.Equal(t, "", w.Body.String())
	assert.Equal(t, []string{routeIndex}, w.HeaderMap["Location"])
}

func TestEditUser(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	salt, hash := generateHash("oldPassword")
	u := &user{Username: "Old Username", Email: "Old Email", Hash: hash, Salt: salt}
	db.Save(&u)

	w := httptest.NewRecorder()
	params := url.Values{
		"username":        {"new username"},
		"email":           {"new email"},
		"newPassword":     {"newPassword"},
		"confirmPassword": {"newPassword"},
	}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, fmt.Sprintf("%s%d", routeEditUser, u.ID), body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken(adminUser)))
	app.editUser(w, r)

	assert.Equal(t, "", w.Body.String())
	assert.Equal(t, []string{fmt.Sprintf("%s%d", routeEditUser, u.ID)}, w.HeaderMap["Location"])

	updatedUser := &user{}
	db.First(updatedUser, "id = ?", u.ID)

	assert.Equal(t, "new username", updatedUser.Username)
	assert.Equal(t, "new email", updatedUser.Email)
	assert.True(t, verifyLogin(updatedUser, "newPassword"))
}

func TestEditUserNoPassword(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	salt, hash := generateHash("oldPassword")
	u := &user{Username: "Old Username", Email: "Old Email", Hash: hash, Salt: salt}
	db.Save(&u)

	w := httptest.NewRecorder()
	params := url.Values{
		"username": {"new username"},
		"email":    {"new email"},
	}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, fmt.Sprintf("%s%d", routeEditUser, u.ID), body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken(adminUser)))
	app.editUser(w, r)

	assert.Equal(t, "", w.Body.String())
	assert.Equal(t, []string{fmt.Sprintf("%s%d", routeEditUser, u.ID)}, w.HeaderMap["Location"])

	updatedUser := &user{}
	db.First(updatedUser, "id = ?", u.ID)

	assert.Equal(t, "new username", updatedUser.Username)
	assert.Equal(t, "new email", updatedUser.Email)
	assert.True(t, verifyLogin(updatedUser, "oldPassword"))
}

func TestEditUserPasswordsDontMatch(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	salt, hash := generateHash("oldPassword")
	u := &user{Username: "Old Username", Email: "Old Email", Hash: hash, Salt: salt}
	db.Save(&u)

	w := httptest.NewRecorder()
	params := url.Values{
		"username":        {"new username"},
		"email":           {"new email"},
		"newPassword":     {"newPassword"},
		"confirmPassword": {"newPass"},
	}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, fmt.Sprintf("%s%d", routeEditUser, u.ID), body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken(adminUser)))
	app.editUser(w, r)

	assert.Contains(t, w.Body.String(), passwordsDoNotMatch)

	updatedUser := &user{}
	db.First(updatedUser, "id = ?", u.ID)

	assert.Equal(t, "Old Username", updatedUser.Username)
	assert.Equal(t, "Old Email", updatedUser.Email)
	assert.True(t, verifyLogin(updatedUser, "oldPassword"))
}

func TestEditUserNonAdmin(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	salt, hash := generateHash("oldPassword")
	u := &user{Username: "Old Username", Email: "Old Email", Hash: hash, Salt: salt}
	db.Save(&u)

	w := httptest.NewRecorder()
	params := url.Values{
		"username":        {"new username"},
		"email":           {"new email"},
		"newPassword":     {"newPassword"},
		"confirmPassword": {"newPassword"},
	}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, fmt.Sprintf("%s%d", routeEditUser, u.ID), body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("notAdmin")))
	app.editUser(w, r)

	assert.Equal(t, "", w.Body.String())
	assert.Equal(t, []string{routeIndex}, w.HeaderMap["Location"])
}

func TestEditUserForm(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	salt, hash := generateHash("oldPassword")
	u := &user{Username: "Old Username", Email: "Old Email", Hash: hash, Salt: salt}
	db.Save(&u)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("%s%d", routeEditUser, u.ID), nil)
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken(adminUser)))
	app.editUser(w, r)

	assert.Contains(t, w.Body.String(), "Username")
	assert.Contains(t, w.Body.String(), "Email")
	assert.Contains(t, w.Body.String(), "Password")
}

func TestEditUserFormInvalidUser(t *testing.T) {
	w, databaseFile := getEditUserGetResponse(t, fmt.Sprintf("%sfff", routeEditUser))
	defer os.Remove(databaseFile)

	assertGetRedirect(t, w, routeEditUser)
}

func TestEditUserFormNonExistentUser(t *testing.T) {
	w, databaseFile := getEditUserGetResponse(t, fmt.Sprintf("%s999", routeEditUser))
	defer os.Remove(databaseFile)

	assertGetRedirect(t, w, routeEditUser)
}

func TestEditUserList(t *testing.T) {
	w, databaseFile := getEditUserGetResponse(t, routeEditUser)
	defer os.Remove(databaseFile)

	assert.Contains(t, w.Body.String(), "Old Username")
	assert.Contains(t, w.Body.String(), "New User")
}

func getEditUserGetResponse(t *testing.T, url string) (*httptest.ResponseRecorder, string) {
	app, db, databaseFile := getApp(t)

	salt, hash := generateHash("oldPassword")
	u := &user{Username: "Old Username", Email: "Old Email", Hash: hash, Salt: salt}
	db.Save(&u)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, url, nil)
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken(adminUser)))
	app.editUser(w, r)

	return w, databaseFile
}
