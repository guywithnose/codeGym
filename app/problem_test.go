package app

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"
	"time"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/stretchr/testify/assert"
)

func TestProblemNoLogin(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	p := &problem{
		Name:        "Name",
		Description: "Description",
		Answer:      500,
		StartDate:   time.Now().Add(-1 * time.Hour),
		EndDate:     time.Now().Add(time.Hour),
	}
	db.Save(&p)

	assertRouteReturnsRedirectLoginNoAuth(t, app, fmt.Sprintf("%s%d", routeProblem, p.ID), app.problem)
}

func TestProblem(t *testing.T) {
	app, db, databaseFile := getApp(t)
	tg := &testTelegram{}
	app.telegram = tg
	defer os.Remove(databaseFile)

	u := &user{Username: "un"}
	db.Save(&u)
	p := &problem{
		Name:        "Name",
		Description: "Description",
		Answer:      500,
		StartDate:   time.Now().Add(-1 * time.Hour),
		EndDate:     time.Now().Add(time.Hour),
	}
	db.Save(&p)

	w := httptest.NewRecorder()
	params := url.Values{"answer": {"500"}}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, fmt.Sprintf("%s%d", routeProblem, p.ID), body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("un")))
	app.problem(w, r)

	a := &attempt{}
	db.First(a)

	assert.Equal(t, p.ID, a.ProblemID)
	assert.Equal(t, u.ID, a.UserID)
	assert.Equal(t, 500, a.Value)
	assert.True(t, a.Success)
	assert.Equal(t, []string{"un solved Name"}, tg.messages)
	assert.Contains(t, w.Body.String(), "You solved")
	assert.Contains(t, w.Body.String(), fmt.Sprintf("Available until: %s", formatDate(p.EndDate)))
	assert.NotContains(t, w.Body.String(), "You may submit again in")
}

func TestProblemSharedLinks(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	u := &user{Username: "un"}
	db.Save(&u)
	p := &problem{
		Name:        "Name",
		Description: "Description",
		Answer:      500,
		StartDate:   time.Now().Add(-1 * time.Hour),
		EndDate:     time.Now().Add(time.Hour),
	}
	db.Save(&p)
	a := &attempt{Success: true, ProblemID: p.ID, UserID: u.ID}
	db.Save(&a)
	s := &sharedCode{Link: "https://www.google.com/search?q=how+to+solve+a+problem", ProblemID: p.ID, UserID: u.ID}
	db.Save(&s)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("%s%d", routeProblem, p.ID), nil)
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("un")))
	app.problem(w, r)

	assert.Contains(t, stripWhitespace(w.Body.String()), "<ahref=\"https://www.google.com/search?q=how&#43;to&#43;solve&#43;a&#43;problem\">un</a>")
}

func TestProblemInvalidAnswer(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	u := &user{Username: "un"}
	db.Save(&u)
	p := &problem{
		Name:        "Name",
		Description: "Description",
		Answer:      500,
		StartDate:   time.Now().Add(-1 * time.Hour),
		EndDate:     time.Now().Add(time.Hour),
	}
	db.Save(&p)

	w := httptest.NewRecorder()
	params := url.Values{"answer": {"notAnInt"}}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, fmt.Sprintf("%s%d", routeProblem, p.ID), body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("un")))
	app.problem(w, r)

	a := &attempt{}
	var count int
	db.Model(a).Count(&count)

	assert.Equal(t, 0, count)
	assert.Contains(t, w.Body.String(), invalidAnswer)
	assert.NotContains(t, w.Body.String(), "Solved")
}

func TestProblemWrongAnswer(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	u := &user{Username: "un"}
	db.Save(&u)
	p := &problem{
		Name:        "Name",
		Description: "Description",
		Answer:      500,
		StartDate:   time.Now().Add(-1 * time.Hour),
		EndDate:     time.Now().Add(time.Hour),
	}
	db.Save(&p)

	w := httptest.NewRecorder()
	params := url.Values{"answer": {"499"}}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, fmt.Sprintf("%s%d", routeProblem, p.ID), body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("un")))
	app.problem(w, r)

	a := &attempt{}
	db.First(a)

	assert.Equal(t, p.ID, a.ProblemID)
	assert.Equal(t, u.ID, a.UserID)
	assert.Equal(t, 499, a.Value)
	assert.False(t, a.Success)
	assert.Contains(t, w.Body.String(), invalidAnswer)
	assert.Contains(t, w.Body.String(), "You may submit again in")
	assert.NotContains(t, w.Body.String(), "Submit")
	assert.NotContains(t, w.Body.String(), "Solved")
}

func TestProblemWrongAnswerStartsTimeLimit(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	u := &user{Username: "un"}
	db.Save(&u)
	p := &problem{
		Name:        "Name",
		Description: "Description",
		Answer:      500,
		StartDate:   time.Now().Add(-1 * time.Hour),
		EndDate:     time.Now().Add(time.Hour),
	}
	db.Save(&p)

	w := httptest.NewRecorder()
	params := url.Values{"answer": {"499"}}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, fmt.Sprintf("%s%d", routeProblem, p.ID), body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("un")))
	app.problem(w, r)

	w = httptest.NewRecorder()
	app.problem(w, r)

	a := &attempt{}
	var count int
	db.Model(a).Count(&count)
	db.First(a)

	assert.Equal(t, 1, count)
	assert.Equal(t, p.ID, a.ProblemID)
	assert.Equal(t, u.ID, a.UserID)
	assert.Equal(t, 499, a.Value)
	assert.False(t, a.Success)
	assert.Contains(t, w.Body.String(), "You may submit again in")
	assert.NotContains(t, w.Body.String(), "Solved")
}

func TestProblemFormActive(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	u := &user{Username: "un"}
	db.Save(&u)

	p := &problem{
		Name:        "Name",
		Description: "This is the problem description",
		Answer:      500,
		StartDate:   time.Now().Add(-1 * time.Hour),
		EndDate:     time.Now().Add(time.Hour),
	}
	db.Save(&p)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("%s%d", routeProblem, p.ID), nil)
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("un")))
	app.problem(w, r)
	assert.NotContains(t, w.Body.String(), "Share Your Solution")
	assert.Contains(t, w.Body.String(), "This is the problem description")
	assert.NotContains(t, w.Body.String(), "500")
}

func TestProblemFormSolved(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	u := &user{Username: "un"}
	db.Save(&u)

	p := &problem{
		Name:        "Name",
		Description: "This is the problem description",
		Answer:      500,
		StartDate:   time.Now().Add(-1 * time.Hour),
		EndDate:     time.Now().Add(time.Hour),
	}
	db.Save(&p)

	a := &attempt{
		ProblemID: p.ID,
		UserID:    u.ID,
		Success:   true,
	}
	db.Save(&a)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("%s%d", routeProblem, p.ID), nil)
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("un")))
	app.problem(w, r)
	assert.Contains(t, w.Body.String(), "Share Your Solution")
	assert.Contains(t, w.Body.String(), "Shared Solutions")
	assert.Contains(t, w.Body.String(), "This is the problem description")
	assert.NotContains(t, w.Body.String(), "500")
}

func TestProblemFormSolvedHiddenSolutions(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	u := &user{Username: "un"}
	db.Save(&u)

	p := &problem{
		Name:          "Name",
		Description:   "This is the problem description",
		Answer:        500,
		StartDate:     time.Now().Add(-1 * time.Hour),
		EndDate:       time.Now().Add(time.Hour),
		HideSolutions: true,
	}
	db.Save(&p)

	a := &attempt{
		ProblemID: p.ID,
		UserID:    u.ID,
		Success:   true,
	}
	db.Save(&a)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("%s%d", routeProblem, p.ID), nil)
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("un")))
	app.problem(w, r)
	assert.Contains(t, w.Body.String(), "Share Your Solution")
	assert.NotContains(t, w.Body.String(), "Shared Solutions")
	assert.Contains(t, w.Body.String(), "This is the problem description")
	assert.NotContains(t, w.Body.String(), "500")
}

func TestProblemFormArchive(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	p := &problem{
		Name:        "Name",
		Description: "This is the problem description",
		Answer:      500,
		StartDate:   time.Now().Add(-2 * time.Hour),
		EndDate:     time.Now().Add(-1 * time.Hour),
	}
	db.Save(&p)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("%s%d", routeProblem, p.ID), nil)
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken(adminUser)))
	app.problem(w, r)
	assert.Contains(t, w.Body.String(), "This is the problem description")
	assert.Contains(t, w.Body.String(), "Share Your Solution")
	assert.NotContains(t, w.Body.String(), "500")
}

func TestProblemFormFuture(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	p := &problem{
		Name:        "Name",
		Description: "This is the problem description",
		Answer:      500,
		StartDate:   time.Now().Add(time.Hour),
		EndDate:     time.Now().Add(2 * time.Hour),
	}
	db.Save(&p)

	assertRouteReturnsRedirectIndex(t, app, fmt.Sprintf("%s%d", routeProblem, p.ID), adminUser, app.problem)
}

func TestProblemInvalidProblemId(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	assertRouteReturnsRedirectIndex(t, app, fmt.Sprintf("%sfoo", routeProblem), adminUser, app.problem)
}

func TestProblemNoProblemID(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	assertRouteReturnsRedirectIndex(t, app, routeProblem, adminUser, app.problem)
}

func TestProblemNonExistentProblem(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	assertRouteReturnsRedirectIndex(t, app, fmt.Sprintf("%s1", routeProblem), adminUser, app.problem)
}

func TestProblemSolveList(t *testing.T) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	u := &user{Username: "un"}
	db.Save(&u)
	u2 := &user{Username: "un2"}
	db.Save(&u2)
	u3 := &user{Username: "un3"}
	db.Save(&u3)
	p := &problem{
		Name:        "Name",
		Description: "Description",
		Answer:      500,
		StartDate:   time.Now().Add(-1 * time.Hour),
		EndDate:     time.Now().Add(time.Hour),
	}
	db.Save(&p)

	a := &attempt{
		ProblemID: p.ID,
		UserID:    u3.ID,
		Success:   true,
	}

	db.Save(&a)

	time.Sleep(time.Second)
	a2 := &attempt{
		ProblemID: p.ID,
		UserID:    u.ID,
		Success:   true,
	}

	db.Save(&a2)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("%s%d", routeProblem, p.ID), nil)
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("un")))
	app.problem(w, r)

	assert.Contains(t, w.Body.String(), "title=\"un3<BR>un<BR>\"")
	assert.Contains(t, w.Body.String(), "Total Solves: 2")
}
