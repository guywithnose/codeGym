package app

import (
	"fmt"
	"html/template"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"
)

const dateFormat = "1/2/06"

type byDate []*problem

func (a byDate) Len() int           { return len(a) }
func (a byDate) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a byDate) Less(i, j int) bool { return a[i].StartDate.After(a[j].StartDate) }

func (a *App) editProblem(w http.ResponseWriter, r *http.Request) {
	viewData := a.getTemplateData("editProblem", r)
	if !viewData.IsAdmin {
		http.Redirect(w, r, routeIndex, http.StatusFound)
		return
	}

	lastRouteSegment := strings.Replace(r.URL.String(), routeEditProblem, "", -1)
	if lastRouteSegment == "" {
		sort.Sort(byDate(viewData.AllProblems))
		panicOnError(a.templates.ExecuteTemplate(w, "problemList", viewData))
		return
	}

	problemID, err := strconv.Atoi(lastRouteSegment)
	if err != nil {
		http.Redirect(w, r, routeEditProblem, http.StatusFound)
		return
	}

	for _, p := range viewData.AllProblems {
		if p.ID == uint(problemID) {
			viewData.CurrentProblem = &problem{}
			*viewData.CurrentProblem = *p
			viewData.Inputs = map[string]string{
				"startDate": viewData.CurrentProblem.StartDate.Format(dateFormat),
				"endDate":   viewData.CurrentProblem.EndDate.Format(dateFormat),
			}

			if r.Method == http.MethodPost {
				a.handleEditProblemPost(viewData, r)

				if viewData.Error == "" {
					a.db.Save(viewData.CurrentProblem)
					if p.isActive {
						http.Redirect(w, r, fmt.Sprintf("%s%d", routeProblem, p.ID), http.StatusFound)
					} else {
						http.Redirect(w, r, routeEditProblem, http.StatusFound)
					}
					return
				}
			}

			panicOnError(a.templates.ExecuteTemplate(w, "editProblem", viewData))
			return
		}
	}

	http.Redirect(w, r, routeEditProblem, http.StatusFound)
}

func (a *App) handleEditProblemPost(viewData *templateData, r *http.Request) {
	name := r.FormValue("name")
	description := template.HTML(r.FormValue("description"))
	hideSolutions := r.FormValue("hideSolutions") == "on"
	startDate, err := time.ParseInLocation(dateFormat, r.FormValue("startDate"), a.location)
	if err != nil {
		viewData.Error = badStartDate
	}

	endDate, err := time.ParseInLocation(dateFormat, r.FormValue("endDate"), a.location)
	if err != nil {
		viewData.Error = badEndDate
	}

	answer := r.FormValue("answer")
	answerInt, err := strconv.Atoi(answer)
	if err != nil {
		viewData.Error = invalidAnswer
	}

	viewData.CurrentProblem.Name = name
	viewData.CurrentProblem.Description = description
	viewData.CurrentProblem.Answer = answerInt
	viewData.CurrentProblem.HideSolutions = hideSolutions
	viewData.CurrentProblem.StartDate = startDate
	viewData.CurrentProblem.EndDate = endDate

	viewData.Inputs = map[string]string{
		"startDate": r.FormValue("startDate"),
		"endDate":   r.FormValue("endDate"),
	}
}
