package app

import (
	"fmt"
	"html/template"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/stretchr/testify/assert"
)

func TestNewProblemNoLogin(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	w := httptest.NewRecorder()
	params := url.Values{
		"name":        {"problem name"},
		"description": {"problem description"},
		"answer":      {"300"},
		"startDate":   {"4/7/18"},
		"endDate":     {"4/21/18"},
	}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, routeNewProblem, body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	app.newProblem(w, r)
	assert.Equal(t, "", w.Body.String())
	assert.Equal(t, []string{routeIndex}, w.HeaderMap["Location"])
}

func TestNewProblem(t *testing.T) {
	w, db := getNewProblemResponse(
		t,
		url.Values{
			"name":        {"problem name"},
			"description": {"problem description"},
			"answer":      {"300"},
			"startDate":   {"4/7/18"},
			"endDate":     {"4/21/18"},
		},
	)

	assert.Equal(t, "", w.Body.String())
	assert.Equal(t, []string{fmt.Sprintf("%s1", routeProblem)}, w.HeaderMap["Location"])

	var p problem
	db.First(&p, "id = ?", 1)

	assert.Equal(t, "problem name", p.Name)
	assert.Equal(t, template.HTML("problem description"), p.Description)
}

func TestNewProblemBadAnswer(t *testing.T) {
	w, _ := getNewProblemResponse(
		t,
		url.Values{
			"name":        {"problem name"},
			"description": {"problem description"},
			"answer":      {"notAnInt"},
			"startDate":   {"4/7/18"},
			"endDate":     {"4/21/18"},
		},
	)
	assert.Contains(t, w.Body.String(), invalidAnswer)
}

func TestNewProblemBadStartDate(t *testing.T) {
	w, _ := getNewProblemResponse(
		t,
		url.Values{
			"name":        {"problem name"},
			"description": {"problem description"},
			"answer":      {"300"},
			"startDate":   {"notADate"},
			"endDate":     {"4/21/18"},
		},
	)
	assert.Contains(t, w.Body.String(), badStartDate)
}

func TestNewProblemBadEndDate(t *testing.T) {
	w, _ := getNewProblemResponse(
		t,
		url.Values{
			"name":        {"problem name"},
			"description": {"problem description"},
			"answer":      {"300"},
			"startDate":   {"4/7/18"},
			"endDate":     {"notADate"},
		},
	)
	assert.Contains(t, w.Body.String(), badEndDate)
	assert.Contains(t, w.Body.String(), "name=\"answer\" value=\"300\"")
}

func TestNewProblemNonAdmin(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	w := httptest.NewRecorder()
	params := url.Values{
		"name":        {"problem name"},
		"description": {"problem description"},
		"answer":      {"notAnInt"},
		"startDate":   {"4/7/18"},
		"endDate":     {"4/21/18"},
	}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, routeNewProblem, body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken("notAdmin")))
	app.newProblem(w, r)
	assert.Equal(t, "", w.Body.String())
	assert.Equal(t, []string{routeIndex}, w.HeaderMap["Location"])
}

func TestNewProblemForm(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, routeNewProblem, nil)
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken(adminUser)))
	app.newProblem(w, r)
	assert.Contains(t, w.Body.String(), "Name")
}

func getNewProblemResponse(t *testing.T, params url.Values) (*httptest.ResponseRecorder, *gorm.DB) {
	app, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	w := httptest.NewRecorder()
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, routeNewProblem, body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken(adminUser)))
	app.newProblem(w, r)

	return w, db
}
