package app

import (
	"html/template"
	"net/http"
	"time"

	"gitlab.com/guywithnose/codeGym/static"
)

type share struct {
	Username string
	Link     string
}

type score struct {
	Username string
	Solves   int
}

type problemSolve struct {
	ProblemName string
	Date        *time.Time
}

type templateData struct {
	ActiveProblems         []*problem
	AllProblems            []*problem
	AllUsers               []*user
	ArchivedProblems       []*problem
	CurrentProblem         *problem
	CurrentUser            *user
	Inputs                 map[string]string
	LastAttempt            *attempt
	Scores                 []score
	Shares                 []share
	ProblemSolves          []string
	Solves                 []problemSolve
	User                   *user
	Error                  string
	NextSubmissionDuration string
	PageName               string
	AppName                string
	CanSubmitProblem       bool
	IsAdmin                bool
	IsArchivedProblem      bool
	ShowScoreboard         bool
}

func (a *App) getTemplateData(name string, r *http.Request) *templateData {
	viewData := &templateData{PageName: name, AppName: a.appName, ShowScoreboard: !a.hideScoreboard}
	user, err := a.checkLogin(r)
	if err == nil {
		viewData.User = user
		viewData.IsAdmin = user.Username == adminUser
	}

	a.db.Order("name").Find(&viewData.AllProblems)

	now := time.Now().In(a.location)

	for _, problem := range viewData.AllProblems {
		if now.After(problem.StartDate) && now.Before(problem.EndDate) {
			problem.isActive = true
			viewData.ActiveProblems = append(viewData.ActiveProblems, problem)
		} else if now.After(problem.EndDate) {
			problem.isArchived = true
			viewData.ArchivedProblems = append(viewData.ArchivedProblems, problem)
		}
	}

	return viewData
}

func getTemplates(environment string) (*template.Template, error) {
	t := template.New("")

	t.Funcs(
		template.FuncMap{
			"date":         formatDate,
			"simpleDate":   simpleDate,
			"calendarDate": calendarDate,
			"after":        after,
			"before":       before,
		},
	)

	templateFiles := []string{
		"/templates/changePassword.tmpl",
		"/templates/editProblem.tmpl",
		"/templates/editUser.tmpl",
		"/templates/error.tmpl",
		"/templates/head.tmpl",
		"/templates/index.tmpl",
		"/templates/login.tmpl",
		"/templates/nav.tmpl",
		"/templates/newProblem.tmpl",
		"/templates/newUser.tmpl",
		"/templates/problemDetail.tmpl",
		"/templates/problemList.tmpl",
		"/templates/resetPasswordSubmitted.tmpl",
		"/templates/resetPassword.tmpl",
		"/templates/resetPasswordWithToken.tmpl",
		"/templates/scoreBoard.tmpl",
		"/templates/scoreBoardUser.tmpl",
		"/templates/shareCode.tmpl",
		"/templates/userList.tmpl",
		"/templates/calendar.tmpl",
	}

	for _, fileName := range templateFiles {
		_, err := t.Parse(static.FSMustString(environment != "production", fileName))
		if err != nil {
			return nil, err
		}
	}

	return t, nil
}

func formatDate(t time.Time) string {
	timeLocation, err := time.LoadLocation("US/Eastern")
	panicOnError(err)

	return t.In(timeLocation).Format("Monday, January 2, 2006 at 15:04:05")
}

func simpleDate(t time.Time) string {
	timeLocation, err := time.LoadLocation("US/Eastern")
	panicOnError(err)

	return t.In(timeLocation).Format("01/02/06")
}

func calendarDate(t time.Time) string {
	timeLocation, err := time.LoadLocation("US/Eastern")
	panicOnError(err)

	return t.In(timeLocation).Format("2006-01-02")
}

func after(t time.Time) bool {
	return time.Now().After(t)
}

func before(t time.Time) bool {
	return t.After(time.Now())
}
