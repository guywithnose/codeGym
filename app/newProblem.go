package app

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"time"
)

func (a *App) newProblem(w http.ResponseWriter, r *http.Request) {
	viewData := a.getTemplateData("newProblem", r)
	if !viewData.IsAdmin {
		http.Redirect(w, r, routeIndex, http.StatusFound)
		return
	}

	if r.Method == http.MethodPost {
		name := r.FormValue("name")
		hideSolutions := r.FormValue("hideSolutions") == "on"
		description := template.HTML(r.FormValue("description"))
		startDate, err := time.ParseInLocation(dateFormat, r.FormValue("startDate"), a.location)
		if err != nil {
			viewData.Error = badStartDate
		}

		endDate, err := time.ParseInLocation(dateFormat, r.FormValue("endDate"), a.location)
		if err != nil {
			viewData.Error = badEndDate
		}

		answer := r.FormValue("answer")
		answerInt, err := strconv.Atoi(answer)
		if err != nil {
			viewData.Error = invalidAnswer
		}

		if viewData.Error == "" {
			p := &problem{Name: name, Description: description, Answer: answerInt, StartDate: startDate, EndDate: endDate, HideSolutions: hideSolutions}
			a.db.Save(&p)
			http.Redirect(w, r, fmt.Sprintf("%s%d", routeProblem, p.ID), http.StatusFound)
			return
		}

		viewData.Inputs = map[string]string{
			"name":        name,
			"description": string(description),
			"answer":      answer,
			"startDate":   r.FormValue("startDate"),
			"endDate":     r.FormValue("endDate"),
		}
	}

	panicOnError(a.templates.ExecuteTemplate(w, "newProblem", viewData))
}
