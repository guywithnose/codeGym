package app

import (
	"html/template"
	"time"

	"github.com/jinzhu/gorm"
)

type user struct {
	gorm.Model
	Username string `gorm:"unique_index"`
	Email    string
	Salt     string
	Hash     string
	IsAdmin  bool
}

type attempt struct {
	gorm.Model
	ProblemID uint
	Value     int
	Success   bool
	UserID    uint
}

type problem struct {
	gorm.Model
	Name          string `gorm:"unique_index"`
	Description   template.HTML
	StartDate     time.Time
	EndDate       time.Time
	Answer        int
	isActive      bool
	isArchived    bool
	HideSolutions bool
}

type sharedCode struct {
	gorm.Model
	ProblemID uint `gorm:"unique_index:problem_user"`
	UserID    uint `gorm:"unique_index:problem_user"`
	Link      string
}

func getMappedUsers(db *gorm.DB) map[uint]string {
	var users []user
	db.Find(&users)

	mappedUsers := map[uint]string{}
	for _, u := range users {
		mappedUsers[u.ID] = u.Username
	}

	return mappedUsers
}

func getMappedProblems(problems []*problem) map[uint]string {
	mappedProblems := map[uint]string{}
	for _, p := range problems {
		mappedProblems[p.ID] = p.Name
	}

	return mappedProblems
}
