package app

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestLoginSuccess(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	w := httptest.NewRecorder()
	params := url.Values{"username": {"admin"}, "password": {"adminPassword"}}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, routeLogin, body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	app.login(w, r)
	assert.Equal(t, "", w.Body.String())
	require.Equal(t, 1, len(w.HeaderMap["Set-Cookie"]))
	assert.Contains(t, w.HeaderMap["Set-Cookie"][0], fmt.Sprintf("%s=", authCookieName))
	assert.Equal(t, []string{routeIndex}, w.HeaderMap["Location"])
}

func TestLoginFail(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	w := httptest.NewRecorder()
	params := url.Values{"username": {"admin"}, "password": {"adminpassword"}}
	body := strings.NewReader(params.Encode())
	r := httptest.NewRequest(http.MethodPost, routeLogin, body)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	app.login(w, r)
	assert.Contains(t, w.Body.String(), "Invalid username or password")
	assert.Equal(t, 0, len(w.HeaderMap["Set-Cookie"]))
}

func TestLoginForm(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, routeLogin, nil)
	app.login(w, r)
	assert.Contains(t, w.Body.String(), "Username")
}

func TestLoginAlreadyAuthenticated(t *testing.T) {
	app, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	assertRouteReturnsRedirectIndex(t, app, routeLogin, adminUser, app.login)
}
