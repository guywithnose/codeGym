package app

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/stretchr/testify/assert"
	"golang.org/x/crypto/scrypt"
)

func TestChangeAdminPassword(t *testing.T) {
	_, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	var admin user
	db.First(&admin, "username = ?", adminUser)
	hash, err := scrypt.Key([]byte("adminPassword"), []byte(admin.Salt), 1<<14, 8, 1, hashLength)
	assert.Nil(t, err)

	assert.Equal(t, []byte(admin.Hash), hash)
}

func TestAdminPasswordNoChangeOnEmpty(t *testing.T) {
	_, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	var admin user
	db.First(&admin, "username = ?", adminUser)
	hash := admin.Hash

	New("", databaseFile, "Code Gym", "production", "", "testDomain.com", getTestCache(), nil, time.Local, nil, false, []string{})
	db.First(&admin, "username = ?", adminUser)
	assert.Equal(t, admin.Hash, hash)
}

func TestUpdateAdminPassword(t *testing.T) {
	_, db, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	var admin user
	db.First(&admin, "username = ?", adminUser)
	hash := admin.Hash

	New("", databaseFile, "Code Gym", "production", "newPassword", "testDomain.com", getTestCache(), nil, time.Local, nil, false, []string{})
	db.First(&admin, "username = ?", adminUser)
	assert.NotEqual(t, admin.Hash, hash)
}

func TestCheckLoginBadToken(t *testing.T) {
	a, _, databaseFile := getApp(t)
	defer os.Remove(databaseFile)

	r := httptest.NewRequest(http.MethodGet, routeIndex, nil)
	r.Header.Set("Cookie", fmt.Sprintf("%s=badToken", authCookieName))
	_, err := a.checkLogin(r)
	assert.NotNil(t, err)
}
