package app

import (
	"crypto/rand"
	"encoding/base64"
	"io"
	"net/http"
	"os"
	"time"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/scrypt"
)

func verifyLogin(u *user, password string) bool {
	hash, err := scrypt.Key([]byte(password), []byte(u.Salt), 1<<14, 8, 1, hashLength)
	panicOnError(err)

	return string(hash) == u.Hash
}

func (a *App) generateAuthCookie(username string) *http.Cookie {
	token := make([]byte, tokenLength)
	_, err := io.ReadFull(rand.Reader, token)
	panicOnError(err)

	tokenString := base64.StdEncoding.EncodeToString(token)
	a.cache.Set(tokenString, username, tokenLifetime)
	return &http.Cookie{
		Name:     authCookieName,
		Value:    tokenString,
		HttpOnly: true,
		Expires:  time.Now().Add(tokenLifetime),
		Path:     "/",
		Secure:   os.Getenv("SECURE_COOKIES") == "true",
	}
}

func (a *App) checkLogin(r *http.Request) (*user, error) {
	authCookie, err := r.Cookie(authCookieName)
	if err != nil {
		return nil, err
	}

	username, err := a.cache.Get(authCookie.Value).Result()
	if err != nil {
		return nil, err
	}

	var u user
	a.db.First(&u, "username = ?", username)

	return &u, nil
}

func updateAdminPassword(db *gorm.DB, password string) {
	update := true
	if password == "" {
		password = "admin"
		update = false
	}

	salt, hash := generateHash(password)

	var admin user
	err := db.First(&admin, "username = ?", adminUser).Error
	if err != nil {
		db.Create(&user{IsAdmin: true, Username: adminUser, Salt: salt, Hash: hash})
		return
	}

	if update {
		admin.Salt = salt
		admin.Hash = hash
		db.Save(admin)
	}
}

func generateHash(password string) (string, string) {
	salt := make([]byte, saltLength)
	_, err := io.ReadFull(rand.Reader, salt)
	panicOnError(err)

	hash, err := scrypt.Key([]byte(password), salt, 1<<14, 8, 1, hashLength)
	panicOnError(err)

	return string(salt), string(hash)
}
