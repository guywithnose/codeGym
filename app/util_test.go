package app

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
	mathRand "math/rand"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"time"
	"unicode"

	"github.com/go-redis/redis"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

func getApp(t *testing.T) (*App, *gorm.DB, string) {
	db, databaseFile := getDb(t)
	app := New("", databaseFile, "Code Gym", "production", "adminPassword", "testDomain.com", getTestCache(), nil, time.Local, nil, false, []string{})
	return app, db, databaseFile
}

func getDb(t *testing.T) (*gorm.DB, string) {
	databaseFile := fmt.Sprintf("%s/codeGymTestDatabase%d.sqlite", os.TempDir(), mathRand.Uint64())
	os.Remove(databaseFile)

	db, err := gorm.Open("sqlite3", databaseFile)
	assert.Nil(t, err)

	db.AutoMigrate(
		&user{},
		&problem{},
		&attempt{},
	)

	return db, databaseFile
}

func (a *App) getLoginToken(username string) string {
	token := make([]byte, tokenLength)
	_, err := io.ReadFull(rand.Reader, token)
	panicOnError(err)

	tokenString := base64.StdEncoding.EncodeToString(token)
	a.cache.Set(tokenString, username, tokenLifetime)
	return tokenString
}

func getTestCache() *redis.Client {
	return redis.NewClient(&redis.Options{Addr: "localhost:6379", DB: 1})
}

type message struct {
	subject string
	body    string
	to      string
}

type testMailer struct {
	messages []message
}

func (tm *testMailer) Send(subject, body, to string) {
	tm.messages = append(tm.messages, message{subject, body, to})
}

type testTelegram struct {
	messages []string
}

func (tg *testTelegram) Send(text string) {
	tg.messages = append(tg.messages, text)
}

func stripWhitespace(input string) string {
	return strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			return -1
		}
		return r
	}, input)
}

func assertRouteReturnsRedirectIndex(t *testing.T, app *App, route, user string, controller func(http.ResponseWriter, *http.Request)) {
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, route, nil)
	r.Header.Set("Cookie", fmt.Sprintf("%s=%s", authCookieName, app.getLoginToken(user)))
	controller(w, r)
	assertGetRedirect(t, w, routeIndex)
}

func assertRouteReturnsRedirectLoginNoAuth(t *testing.T, app *App, route string, controller func(http.ResponseWriter, *http.Request)) {
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, route, nil)
	controller(w, r)
	assertGetRedirect(t, w, routeLogin)
}

func assertGetRedirect(t *testing.T, w *httptest.ResponseRecorder, route string) {
	assert.Equal(t, fmt.Sprintf("<a href=\"%s\">Found</a>.\n\n", route), w.Body.String())
	assert.Equal(t, []string{route}, w.HeaderMap["Location"])
}
