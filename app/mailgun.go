package app

import (
	"fmt"

	mailgun "gopkg.in/mailgun/mailgun-go.v1"
)

type MailGunMailerInterface interface {
	Send(subject, body, to string)
}

type mailGunMailer struct {
	mg        mailgun.Mailgun
	fromEmail string
}

func NewMailGunMailer(domain, apiKey string) *mailGunMailer {
	return &mailGunMailer{mailgun.NewMailgun(domain, apiKey, ""), fmt.Sprintf("resetPassword@%s", domain)}
}

func (mailer mailGunMailer) Send(subject, body, to string) {
	message := mailgun.NewMessage(mailer.fromEmail, subject, body, to)
	_, _, _ = mailer.mg.Send(message)
}
