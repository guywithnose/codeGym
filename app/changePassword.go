package app

import (
	"net/http"
)

func (a *App) changePassword(w http.ResponseWriter, r *http.Request) {
	viewData := a.getTemplateData("changePassword", r)
	if viewData.User == nil {
		http.Redirect(w, r, routeIndex, http.StatusFound)
		return
	}

	if r.Method == http.MethodPost {
		oldPassword := r.FormValue("oldPassword")
		newPassword := r.FormValue("newPassword")
		confirmPassword := r.FormValue("confirmPassword")
		if newPassword != confirmPassword {
			viewData.Error = passwordsDoNotMatch
		} else {
			if !verifyLogin(viewData.User, oldPassword) {
				viewData.Error = wrongPassword
			} else {
				salt, hash := generateHash(newPassword)
				viewData.User.Salt = salt
				viewData.User.Hash = hash
				a.db.Save(&viewData.User)
				http.Redirect(w, r, routeIndex, http.StatusFound)
				return
			}
		}
	}

	panicOnError(a.templates.ExecuteTemplate(w, "changePassword", viewData))
}
