package app

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
	"net/http"
	"net/url"
)

func (a *App) resetPassword(w http.ResponseWriter, r *http.Request) {
	viewData := a.getTemplateData("resetPassword", r)
	if viewData.User != nil {
		http.Redirect(w, r, routeIndex, http.StatusFound)
		return
	}

	token := r.URL.Query()["token"]

	if len(token) == 1 {
		tokenValue, err := a.cache.Get(token[0]).Result()
		if err != nil || tokenValue[:6] != "reset-" {
			http.Redirect(w, r, routeIndex, http.StatusFound)
			return
		}

		username := tokenValue[6:]
		u := &user{}
		err = a.db.First(u, "username = ?", username).Error
		if err != nil {
			http.Redirect(w, r, routeIndex, http.StatusFound)
			return
		}

		if r.Method == http.MethodPost {
			newPassword := r.FormValue("newPassword")
			confirmPassword := r.FormValue("confirmPassword")
			if newPassword != confirmPassword {
				viewData.Error = passwordsDoNotMatch
			} else {
				salt, hash := generateHash(newPassword)
				u.Salt = salt
				u.Hash = hash
				a.db.Save(&u)

				http.SetCookie(w, a.generateAuthCookie(u.Username))
				http.Redirect(w, r, routeIndex, http.StatusFound)
				return
			}
		}

		panicOnError(a.templates.ExecuteTemplate(w, "resetPasswordWithToken", viewData))
		return
	}

	if r.Method == http.MethodPost {
		username := r.FormValue("username")

		u := &user{}
		err := a.db.First(u, "username = ? or email = ?", username, username).Error

		if err == nil {
			a.sendResetLink(u)
		}

		panicOnError(a.templates.ExecuteTemplate(w, "resetPasswordSubmitted", viewData))
		return
	}

	panicOnError(a.templates.ExecuteTemplate(w, "resetPassword", viewData))
}

func (a *App) sendResetLink(u *user) {
	token := make([]byte, tokenLength)
	_, err := io.ReadFull(rand.Reader, token)
	panicOnError(err)
	tokenString := base64.StdEncoding.EncodeToString(token)
	a.cache.Set(tokenString, fmt.Sprintf("reset-%s", u.Username), tokenLifetime)
	link := fmt.Sprintf("%s?%s", routeResetPasword, url.Values{"token": {tokenString}}.Encode())
	a.mailer.Send("Reset Password", fmt.Sprintf("Go to %s%s to reset your password.", a.baseURL, link), u.Email)
}
