package app

import (
	"fmt"
	"net/http"
)

func (a *App) newUser(w http.ResponseWriter, r *http.Request) {
	viewData := a.getTemplateData("newUser", r)
	if r.Method == http.MethodPost {
		username := r.FormValue("username")
		if !a.validateUsername(username) {
			viewData.Error = unableToCreateUser
		} else {
			email := fmt.Sprintf("%s@sezzle.com", username)
			u := &user{Username: username, Email: email}
			a.db.Save(&u)
			a.sendResetLink(u)
			viewData.Error = userCreated
		}
	}

	panicOnError(a.templates.ExecuteTemplate(w, "newUser", viewData))
}

func (a *App) validateUsername(username string) bool {
	for _, validUser := range a.validUsers {
		if username == validUser {
			return true
		}
	}

	return false
}
