package app

import (
	"fmt"
	"net/http"
	"strconv"
)

func (a *App) shareCode(w http.ResponseWriter, r *http.Request) {
	viewData := a.getTemplateData("shareCode", r)

	if viewData.User == nil {
		http.Redirect(w, r, routeLogin, http.StatusFound)
		return
	}

	if r.Method != http.MethodPost {
		http.Redirect(w, r, routeIndex, http.StatusFound)
		return
	}

	link := r.FormValue("link")
	problemID, err := strconv.Atoi(r.FormValue("problemID"))
	if err != nil || problemID < 0 {
		http.Redirect(w, r, routeIndex, http.StatusFound)
		return
	}

	if link != "" {
		sc := &sharedCode{ProblemID: uint(problemID), UserID: viewData.User.ID}
		a.db.First(sc, "problem_id = ? and user_id = ?", problemID, viewData.User.ID)
		sc.Link = link
		a.db.Save(sc)
	}

	http.Redirect(w, r, fmt.Sprintf("%s%d", routeProblem, problemID), http.StatusFound)
}
